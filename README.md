# README #


### What is this repository for? ###
* This repo is for FoodSafety team members to document and maintain features in the FoodSafety application
* v1.0.0.0

### How do I get set up? ###

* Get repo

### Contribution guidelines ###

* When questions are asked, or feature documentation is lacking, write a quick summary to help others in the future.
* First summarize feature 
* Write steps for testing and/or execution.

### Who do I talk to? ###

* Dylan Justice <dylan_justice@compaid.com>
* Other community or team contact